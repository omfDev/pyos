# !/usr/bin/python
# pyos is a Module PyOs is an Module interacting with the Operating System
# https://gitlab.com/omfDev/pyos
"""PyOs Linux(Posix Version)

PyOs is an Pyhon Module(Mostly Wrapper)
for interactingwith the Operating System and especially it File system.
Developed for OMF.
https://gitlab.com/omfDev/pyos
Version 1.0
"""
import os
import random


def create_file(path):
    """ Create File

    Parameters
    ---
    path : str,
        The Path including name of the File
    """
    os.mknod(path)


def get_random_file(path, extension=False):
        """Returns a Random File given the Path.

        Parameters
        ---
        path : str,
            path from which directory a random file shall be taken

        extension : list, optional (don't work')
            include extension which shall be used like .png and .gif

        Return
        ---
        str,
            a string containing the File Path

        """
        files = os.listdir(path)
        # Check for extension but will pop itself out
        # for file in range(0, len(files)):
        #    if extension is not False:
        #        if files[file].lower().endswith(
        #            (extension)
        #        )is False:
        #            files.pop(file)
        index = random.randrange(0, len(files))
        return files[index]


def get_dir_size(dir):
    """ Returns Size(amount of files of given directory

    Parameters
    ---
    dir : str,
        path of directory

    Return
    ---
    int, the size of dir
    """

    return len(os.listdir(dir))


def clear_dir(dir):
    """ Clear Directory

    Parameters
    ---
    dir : str,
        path of Directory

    """
    os.system('rm %s/*' % dir)


def remove_file(file):
    """ Remove File (also removes Directorys)

    Parameters
    ---
    file : str,
        path of file

    """
    os.system('rm -rf %s' % file)


def create_dir(path):
    """ Create directory

    Parameters
    ---
    path : str,
        Path of directory

    """
    os.system('mkdir %s' % path)


def convert_gif_to_avi(input, output='Overwrite'):
    """ Convert .gif to .avi using ffmpeg

    Parameters
    ---
    input: str,
        input gif filepath
    output: str, optional
        output avi filepath default just the input with .avi

    Return
    ---
    output : str,
        output file path
    """
    if output is 'Overwrite':
        output = os.path._splitext(input)[0] + '.avi'
    os.system(
        'ffmpeg -i %s -movflags faststart -pix_fmt yuv420p \
        -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" %s.avi'
        % (input, output))
    return output


def get_file_extension(file):
    """ Returns the File type using it extenstion

    Parameters
    ---
    file : str,
        Input Filepath including extension

    Return
    ---
    file_extension : str,
        File Extension like '.gif'

    """
    file_extension = os.path.splitext(file)[1]
    return file_extension


def rezise_image(image_input, x_size, y_size, image_output='Overwrite'):
    """ Rezise image

    Parameters
    ---
    image_input : str,
        image path for input
    x_size : int,
        image x size in pixel
    y_size : int,
        image y size in pixel
    image_output : str, optional
        image output path default=image_input

    Return
    ---
    image_output : str,
        output file path
    """
    if image_output == 'Overwrite':
        image_output = image_output
    os.system(
        'convert ' + image_input + ' -resize %sx%s ! ' + image_output
        % x_size, y_size)
